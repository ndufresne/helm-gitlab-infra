#!/bin/sh

# Inspired by https://github.com/moparisthebest/static-curl/
# Minor modifications by: Martin Roukala <martin.roukala@mupuf.org>

# to test locally, run:
# docker run --rm -v $(pwd)/.gitlab-ci/jq-build.sh:/build.sh -w /tmp -e ARCH=amd64 alpine:latest /build.sh

set -exu

apk add git
git clone --recurse-submodules https://github.com/stedolan/jq.git jq
cd jq

# Verify that the binary does not current exist
URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/jq/$(git describe --tags)/jq-${ARCH}"
if wget --spider "$URL" 2>/dev/null; then
    echo "The binary already exists, abort!"
    exit 0
fi

# Install all the build deps
apk add build-base libtool autoconf automake curl

autoreconf -fi
./configure --with-oniguruma=builtin --disable-maintainer-mode
make LDFLAGS=-all-static -j4
strip jq

# print out some info about this, size, and to ensure it's actually fully static
ls -lah jq
file jq
# exit with error code 1 if the executable is dynamic, not static
ldd jq && exit 1 || true

./jq -V

# Upload the generated binary when merged in the main branch
if [ "${CI_COMMIT_BRANCH-premerge}" == "${CI_DEFAULT_BRANCH-notingitlab}" ]; then
    # running on the default branch. CI_COMMIT_BRANCH is *not* set in MRs
    curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "./jq" "${URL}"
else
    echo -e "\n\nLet's just verify that it can parse basic offline information:"
    if [ $(echo '{"id":12345}' | ./jq .id) == "12345" ]; then
        echo "SUCCESS!"
        exit 0
    else
        echo "FAILURE!"
        exit 1
    fi
fi
